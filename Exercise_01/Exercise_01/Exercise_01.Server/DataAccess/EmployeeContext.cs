﻿using Exercise_01.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorCrud.Server.DataAccess
{
    public class EmployeeContext : DbContext
    {
        public DbSet<Employee> tblEmployee { get; set; }
        public DbSet<Department> tblDepartment { get; set; }
        public EmployeeContext(DbContextOptions<EmployeeContext> options) : base(options) { }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Employee>().ToTable("KSZ_Employee");
            modelBuilder.Entity<Department>().ToTable("KSZ_Department");
        }
    }
}