﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_01.Shared
{
    public class Employee
    {
        [Key]
        public int ID { get; set; }
        public int DepartmentID { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }
}