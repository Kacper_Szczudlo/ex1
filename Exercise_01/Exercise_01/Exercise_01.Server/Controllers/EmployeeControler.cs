﻿using BlazorCrud.Server.DataAccess;
using Exercise_01.Shared;
using Microsoft.AspNetCore.Mvc;
using Rksoftware.PropertyCopy;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Exercise_01.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private EmployeeContext context;
        public EmployeeController(EmployeeContext _context)
        {
            context = _context;
        }

        [HttpGet]
        public ActionResult<List<Employee>> Get()
        {
            return context.tblEmployee.ToList();
        }
        [HttpPost]
        public ActionResult Post(Employee employee)
        {
            try
            {
                if (employee is null)
                {
                    throw new ArgumentNullException(nameof(employee));
                }
                context.tblEmployee.Add(employee);
                context.SaveChanges();
                return StatusCode(201);
            }
            catch
            {
                return StatusCode(400);
            }
        }
        [HttpPut("{id}")]
        public ActionResult Put([FromBody] Employee employee, int id)
        {
            try
            {
                if (employee is null)
                {
                    throw new ArgumentNullException(nameof(employee));
                }
                var exist = context.tblEmployee.Where(o => o.ID == id).FirstOrDefault();
                PropertyCopier.CopyTo(employee, exist);
                if (exist != null)
                {
                    context.SaveChanges();
                    return StatusCode(201);
                }
                else
                {
                    return StatusCode(404);
                }
            }
            catch
            {
                return StatusCode(400);
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                context.Remove(context.tblEmployee.Where(o => o.ID == id).FirstOrDefault());
                context.SaveChanges();
                return StatusCode(201);
            }
            catch
            {
                return StatusCode(400);
            }
        }
    }
}
