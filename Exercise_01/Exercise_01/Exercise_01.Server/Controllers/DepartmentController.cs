﻿using BlazorCrud.Server.DataAccess;
using Exercise_01.Shared;
using Microsoft.AspNetCore.Mvc;
using Rksoftware.PropertyCopy;
using System;
using System.Collections.Generic;
using System.Linq;
namespace Exercise_01.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private EmployeeContext context;
        public DepartmentController(EmployeeContext _context)
        {
            context = _context;
        }
        [HttpGet("{id}")]
        public ActionResult<Department> Get(int id)
        {
            Department department = new Department();
            var exist = context.tblEmployee.Where(o => o.ID == id).FirstOrDefault();
            if (exist is not null)
            {
                department.Employes = context.tblEmployee.Where(o => o.DepartmentID == department.ID).ToList();
            }
            return department;
        }
        [HttpGet]
        public ActionResult<List<Department>> Get()
        {
            IEnumerable<Department> departments = context.tblDepartment;
            List<Department> newDepartments = new List<Department>();
            try
            {
                for (var i = 0; i < departments.Count(); i++)
                {
                    if (context.tblEmployee.Where(o => o.DepartmentID == i) is not null)
                    {
                        //newDepartments.Where(o => o.ID == i).FirstOrDefault().Employess = context.Employees.Where(o => o.DepartmentID == i).ToList();
                    }
                }
            }
            catch
            {
                return StatusCode(400);
            }
            return departments.ToList();
        }
        [HttpPost]
        public ActionResult Post(Department department)
        {
            try
            {
                if (department is null)
                {
                    throw new ArgumentNullException(nameof(department));
                }
                context.tblDepartment.Add(department);
                context.SaveChanges();
                return StatusCode(201);
            }
            catch
            {
                return StatusCode(400);
            }
        }
        [HttpPut("{id}")]
        public ActionResult Put([FromBody] Department department, int id)
        {
            try
            {
                if (department is null)
                {
                    throw new ArgumentNullException(nameof(department));
                }
                var exist = context.tblDepartment.Where(o => o.ID == id).FirstOrDefault();
                PropertyCopier.CopyTo(department, exist);
                if (exist != null)
                {
                    context.SaveChanges();
                    return StatusCode(201);
                }
                else
                {
                    return StatusCode(404);
                }
            }
            catch
            {
                return StatusCode(400);
            }
        }
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                context.Remove(context.tblDepartment.Where(o => o.ID == id).FirstOrDefault());
                context.SaveChanges();
                return StatusCode(201);
            }
            catch
            {
                return StatusCode(400);
            }
        }
    }
}