﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Exercise_01.Server.Migrations
{
    public partial class first : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "KSZ_Department",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KSZ_Department", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "KSZ_Employee",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DepartmentID = table.Column<int>(type: "int", nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Surname = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KSZ_Employee", x => x.ID);
                    table.ForeignKey(
                        name: "FK_KSZ_Employee_KSZ_Department_DepartmentID",
                        column: x => x.DepartmentID,
                        principalTable: "KSZ_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_KSZ_Employee_DepartmentID",
                table: "KSZ_Employee",
                column: "DepartmentID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "KSZ_Employee");

            migrationBuilder.DropTable(
                name: "KSZ_Department");
        }
    }
}
