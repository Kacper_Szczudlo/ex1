﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;
using Exercise_01.Shared;

namespace Exercise_01.Client.Services
{
    public class EmployeeService
    {
        public System.Uri Base;
        async public Task<IEnumerable<Employee>> GetEmployeesEditableAsync(CancellationToken ct = default)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                return await client.GetFromJsonAsync<IEnumerable<Employee>>("");
            }
        }
        async public Task InsertEmployeeAsync(IDictionary<string, object> newValues)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                Dictionary<string, string> dString = newValues.ToDictionary(k => k.Key, k => k.Value.ToString());
                await client.PostAsJsonAsync("", dString);
            }
        }
        public Task InsertEmployeeAsync(Employee dataItem)
        {
            return null;
        }
        async public Task RemoveEmployeeAsync(Employee dataItem)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                var res = await client.DeleteAsync("https://localhost:44316/api/employee/" + dataItem.ID);
            }
        }
        async public Task UpdateEmployeeAsync(Employee dataItem, IDictionary<string, object> newValues)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                if (newValues.TryGetValue("DepartmentID", out object DepartmentID))
                {
                    dataItem.DepartmentID = Convert.ToInt32(DepartmentID);
                }
                if (newValues.TryGetValue("FirstName", out object FirstName))
                {
                    dataItem.FirstName = FirstName.ToString();
                }
                if (newValues.TryGetValue("Surname", out object Surname))
                {
                    dataItem.Surname = Surname.ToString();
                }
                var res = await client.PutAsJsonAsync(client.BaseAddress + "/" + dataItem.ID, dataItem);
                Console.WriteLine(client.BaseAddress + "/" + dataItem.ID);
            }
        }

    }
}
